import org.openqa.selenium.*;
import org.junit.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.JavascriptExecutor;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Swiggy {
	private static WebDriver driver;
	List<WebElement> elements;
	List <WebElement> elements2;
	List <WebElement> elements4;
	List <WebElement> addArr;
@BeforeClass
public static void login(){
	System.setProperty("webdriver.gecko.driver","/home/vela-zstk259/Downloads/geckodriver-v0.30.0-linux64/geckodriver");
	driver=new FirefoxDriver();
}
@Test
public void A_Processdish1() throws InterruptedException{
	//login
	driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
	driver.navigate().to("https://www.swiggy.com/");
	driver.findElement(By.linkText("Login")).click();
	driver.findElement(By.id("mobile")).sendKeys("7010083023");
	driver.findElement(By.className("a-ayg")).click();
	
	WebDriverWait wait=new WebDriverWait(driver,60);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("all_restaurants")));
	
	//searchHotel
	driver.findElement(By.linkText("Search")).click();
	driver.findElement(By.className("_2BJMh")).sendKeys("Vairamaaligai");
	driver.findElement(By.className("_34T1N")).click();
	driver.findElement(By.className("_3pTZL")).click();
	elements = driver.findElements(By.className("D_TFT"));
	elements.get(0).click();
	addArr=driver.findElements(By.className("_1RPOp"));
	addArr.get(0).click();
	Thread.sleep(2000);
	System.out.println("1st testcase passed successfully");
}

@Test
public void B_dish2() throws InterruptedException {
	elements = driver.findElements(By.className("D_TFT"));
	elements.get(1).click();
	Thread.sleep(1000);
	addArr=driver.findElements(By.className("_1RPOp"));
	addArr.get(5).click();
	Thread.sleep(2000);
	System.out.println("2nd testcase passed successfully");
}
@Test
public void C_dish3() throws InterruptedException {
	elements = driver.findElements(By.className("D_TFT"));
	elements.get(2).click();
	Thread.sleep(1000);
	addArr=driver.findElements(By.className("_1RPOp"));
	addArr.get(6).click();
	Thread.sleep(2000);
	System.out.println("3rd testcase passed successfully");
}
@Test
public void D_dish4() throws InterruptedException {
	elements = driver.findElements(By.className("D_TFT"));
	elements.get(3).click();
	Thread.sleep(1000);
	addArr=driver.findElements(By.className("_1RPOp"));
	addArr.get(11).click();
	Thread.sleep(2000);
	System.out.println("4th testcase passed successfully");
}
@Test
public void E_dish5() throws InterruptedException {
	elements = driver.findElements(By.className("D_TFT"));
	elements.get(4).click();
	Thread.sleep(1000);
	addArr=driver.findElements(By.className("_1RPOp"));
	addArr.get(13).click();
	Thread.sleep(2000);
	System.out.println("5th testcase passed successfully");
}
@Test
public void F_dish6() throws InterruptedException {
	elements = driver.findElements(By.className("D_TFT"));
	elements.get(5).click();
	Thread.sleep(1000);
	addArr=driver.findElements(By.className("_1RPOp"));
	addArr.get(16).click();
	Thread.sleep(2000);
	driver.findElement(By.className("_1gPB7")).click();
	Thread.sleep(1000);
	System.out.println("6th testcase passed successfully");
}
@Test
public void G_address7() throws InterruptedException {
	driver.findElement(By.className("Ldi91")).click();
	driver.findElement(By.id("building")).sendKeys("33");
	driver.findElement(By.id("landmark")).sendKeys("Tirunelveli-town");
	driver.findElement(By.className("_1dzL9")).click();
	driver.findElement(By.className("_2sd1x")).click();
	Thread.sleep(2000);
	System.out.println("7th testcase passed successfully");
}

@Test
public void H_back8() throws InterruptedException {
	driver.navigate().back();
	Thread.sleep(1000);
	driver.findElement(By.className("_2Epw9")).click();
	Thread.sleep(1000);
	driver.findElement(By.className("_8pSp-")).click();
	System.out.println("8th testcase passed successfully");
}
@Test
public void I_sample6() {	
	driver.findElement(By.className("_8pSp-")).click();
	JavascriptExecutor js=(JavascriptExecutor)driver;
	js.executeScript ("window.scrollBy(0,900)","");
	System.out.println("9th testcase passed successfully");
}
@Test
public void J_offer() throws InterruptedException {
	elements = driver.findElements(By.className("_1fo6c"));
	elements.get(3).click();
	Thread.sleep(1000);
	JavascriptExecutor js=(JavascriptExecutor) driver;
	js.executeScript("window.scrollBy(0,1000)","");
	Thread.sleep(1000);
	System.out.println("10th testcase passed successfully");
}
@Test
public void K_help() throws InterruptedException {
	elements = driver.findElements(By.className("_1fo6c"));
	elements.get(2).click();
	Thread.sleep(1000);
	elements2=driver.findElements(By.className("_2y6HV"));
	elements2.get(1).click();
	Thread.sleep(1000);
	driver.findElement(By.className("_3rl8Q")).click();
	Thread.sleep(2000);
	System.out.println("11th testcase passed successfully");
}
@Test
public void L_profile() throws InterruptedException {
	elements = driver.findElements(By.className("_1fo6c"));
	elements.get(1).click();
	Thread.sleep(1000);
	elements4=driver.findElements(By.className("awo_x"));
	elements4.get(0).click();
	Thread.sleep(1000);
	elements4.get(1).click();
	Thread.sleep(1000);
	elements4.get(2).click();
	Thread.sleep(1000);
	elements4.get(4).click();
	Thread.sleep(1000);
	driver.findElement(By.cssSelector("._2xL-J:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(3) > span:nth-child(2)")).click();
	driver.findElement(By.className("_2-MHS")).click();
    //b0G1m
	driver.findElement(By.className("_3VLw2")).click();
	Thread.sleep(1000);
	driver.findElement(By.className("SSFcO")).click();
	System.out.println("12th testcase passed successfully");
}
@Test
public void M_cart() throws InterruptedException {
	elements = driver.findElements(By.className("_1fo6c"));
	elements.get(0).click();
	Thread.sleep(1000);
	driver.navigate().back();
	Thread.sleep(1000);
	driver.findElement(By.className("_8pSp-")).click();
	System.out.println("13th testcase passed successfully");
}
@AfterClass
   public static void quit() throws InterruptedException {
   Thread.sleep(5000);
   driver.quit();
   System.out.println("Program passed successfully");
}
}

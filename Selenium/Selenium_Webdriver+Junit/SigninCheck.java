import org.junit.*;
import org.openqa.selenium.*;
//import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SigninCheck {
	private static WebDriver driver;
	
@BeforeClass
public static void openBowser()
{
	System.setProperty("webdriver.gecko.driver","/home/vela-zstk259/Downloads/geckodriver-v0.30.0-linux64/geckodriver");					
}   

@Test
 public void A_valid_UserCredential1() throws InterruptedException{
	 driver = new FirefoxDriver();
     driver.get("https://www.google.com");	
     Thread.sleep(2000);
     driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input")).click();
     Thread.sleep(2000);
     driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input")).sendKeys("zohoSchool");
     Thread.sleep(2000);
     driver.findElement(By.linkText("Sign in")).click();
     Thread.sleep(2000);
     driver.findElement(By.id("identifierId")).click();
     Thread.sleep(2000);
     String data=driver.findElement(By.xpath("//*[@id=\"headingSubtext\"]/span")).getText();
     System.out.println(data);
     driver.findElement(By.id("identifierId")).sendKeys("pinkyp@gmail.com");
     Thread.sleep(2000);
     driver.findElement(By.xpath("//*[@id=\"identifierNext\"]/div/button/span")).click();
     Thread.sleep(2000);
     System.out.println("It doesn't take invalid email,so passed!");
     
 }

 @AfterClass
 public static void closeBrowser(){
 driver.quit();
 }
}

import org.testng.annotations.*;
//import org.testng.annotations.BeforeMethod;
//import org.testng.Assert;

import java.util.List;
//import org.testng.annotations.AfterMethod;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.JavascriptExecutor;


public class OlxPage {
	WebDriver driver;
	JavascriptExecutor js;
	List <WebElement> arrArrow;
	List <WebElement> arrPlace;
	
  @Test (priority=5)
  public void title() throws InterruptedException {
	  String GnTitle = "OLX - Free classifieds in India, Buy and Sell for free anywhere in India with OLX Online Classified Advertising";
	  String title = driver.getTitle();
	  System.out.println("Title : "+title+" titlle : "+GnTitle);
	  Thread.sleep(1000);
  }
  
  @Test (priority=1)
  public void language() throws InterruptedException {
	  driver.findElement(By.xpath("//*[@id=\"container\"]/header/div/div/div[3]/div[1]/div")).click();
	  driver.findElement(By.xpath("//*[@id=\"container\"]/header/div/div/div[3]/div[2]/div/ul/li[2]/span")).click();
	  Thread.sleep(2000);
	  System.out.println("The language Changed successfully");
  }
  
  @Test (priority=2)
  public void location() throws InterruptedException {
	 // driver.findElement(By.xpath("/html/body/div[1]/div/header/div/div/div[2]/div/div/div[1]/div/div[1]/span/button")).click();
	//  driver.findElement(By.xpath("//*[@id=\"container\"]/header/div/div/div[2]/div/div/div[1]/div/div[2]/div/div[2]/div[2]/div[1]")).click();
    //  arrArrow=driver.findElements(By.className("_1fwdq"));
//	  arrArrow.get(0).click();    /html/body/div[1]/div/header/div/div/div[2]/div/div/div[1]/div/div[1]/span/button
	 driver.findElement(By.xpath("/html/body/div[1]/div/header/div/div/div[2]/div/div/div[1]/div/div[1]/input")).sendKeys("India");
	  Thread.sleep(2000);
	  //arrPlace=driver.findElements(By.className(""));
	  //arrPlace.get(3).click();
	  //Thread.sleep(2000);
	//  driver.findElement(By.xpath("//*[@id=\"container\"]/header/div/div/div[2]/div/div/div[1]/div/div")).click();
	 // Thread.sleep(2000);
	  js = (JavascriptExecutor) driver;
	  js.executeScript("window.scrollBy(0,450)", "");
	  Thread.sleep(2000);
	  System.out.println("The Location changed successfully");  
  }
  
  @Test (priority=3)
  public void searchBar() throws InterruptedException {
	  js = (JavascriptExecutor) driver;
	  driver.findElement(By.xpath("//*[@id=\"container\"]/header/div/div/div[2]/div/div/div[2]/div/form/fieldset/div/input")).click();
	  driver.findElement(By.xpath("//*[@id=\"container\"]/header/div/div/div[2]/div/div/div[2]/div/form/fieldset/div/input")).sendKeys("Rocket");
	  driver.findElement(By.xpath("//*[@id=\"container\"]/header/div/div/div[2]/div/div/div[3]")).click();
	  Thread.sleep(3000);
	  js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
	  Thread.sleep(2000);
	  System.out.println("The Search Bar working properly");
  }
  
  @Test (priority=4)
  public void loginbtn() throws InterruptedException {
	  driver.findElement(By.xpath("//*[@id=\"container\"]/header/div/div/div[4]/button/span")).click();
	  System.out.println("Login page displayed successfully");
	  Thread.sleep(1000);
  } 
  
  @BeforeTest
  public void start() throws InterruptedException{
	  System.setProperty("webdriver.chrome.driver", "/home/vela-zstk259/Downloads/chromedriver_linux64/chromedriver");
	  driver = new ChromeDriver();
	  driver.navigate().to("https://www.olx.in");
	  Thread.sleep(2000);
	  driver.manage().window().maximize();;
  }
  
  @AfterTest
  public void finish() {
	  System.out.println("Test finished successfully!");
	  driver.close();
	  
  }
}


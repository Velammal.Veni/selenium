
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.*;

public class ZohoShow {
	
	WebDriver driver;
	WebElement check1, check2, sessionName, description;
	String session, desc, SName;
	WebDriverWait wait;
	
  @Test (priority=1)
  
  public void Showtest() throws InterruptedException{
	  
	  wait = new WebDriverWait(driver,20);
	  
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".main-navbar")));
	  
	 if(driver.findElement(By.cssSelector(".main-navbar")).isDisplayed()) {
		 System.out.println("I'm in the correct page");
	 }else {
		 System.out.println("No! This is wrong page");
	 }
	 Thread.sleep(2000);
	 driver.findElement(By.cssSelector(".home-screen")).click();
	 Thread.sleep(1000);
	 sessionName = driver.findElement(By.cssSelector("form>div>input[class='form-control ember-text-field ember-view']"));
	 sessionName.sendKeys("Automation Checking test");
	 Thread.sleep(2000);
	 description = driver.findElement(By.cssSelector("textarea.ember-text-area.form-control.ember-view"));
	 description.sendKeys("Welcome our session for your best learning!");
	 Thread.sleep(2000);
	 check1 = driver.findElement(By.cssSelector("label[for='enableRegistration']"));
	 check1.click();
	 Thread.sleep(1000);
	 driver.findElement(By.cssSelector(".btn.btn-primary.btn-wide.ember-view")).click();
	 Thread.sleep(1000); 
  }
  
  @Test (priority=2)
  
  public void Verification() throws InterruptedException {
	  session = driver.findElement(By.cssSelector(".session-title")).getText();
	  assertEquals(session, "Automation Checking test");
	  System.out.println("Session Name verified successfully");
	  desc = driver.findElement(By.cssSelector(".description-content")).getText();
	  assertEquals(desc, "Welcome our session for your best learning!");
	  System.out.println("Description verified successfully");
	  driver.findElement(By.id("session-registration-tab")).click();
	  Thread.sleep(1000);
	  check2 = driver.findElement(By.cssSelector("label[for='registrationCheckBox']"));
	 // check2.isSelected();
	  System.out.println("The checkbox verification "+ check2.isSelected());
  }
  
  @BeforeTest
  public void beforeTest() throws InterruptedException{
	  
	  System.setProperty("webdriver.chrome.driver", "/home/vela-zstk259/Downloads/chromedriver_linux64/chromedriver");
	  driver = new ChromeDriver();
	  wait = new WebDriverWait(driver,20);
	  driver.get("https://www.zoho.com/showtime/");
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("section[id='block-block-3']>div>div>div>div>a.zgh-login")));
	  Thread.sleep(1000);
	  driver.manage().window().maximize();
	  Thread.sleep(1000);
	  driver.findElement(By.cssSelector("section[id='block-block-3']>div>div>div>div>a.zgh-login")).click();
	  Thread.sleep(2000);
	  driver.findElement(By.id("login_id")).sendKeys("velammal.veni@zohocorp.com");
	  driver.findElement(By.id("login_id")).submit();
	  Thread.sleep(10000);
  }

  @AfterTest
  public void afterTest() {
	  driver.close();
  }

}

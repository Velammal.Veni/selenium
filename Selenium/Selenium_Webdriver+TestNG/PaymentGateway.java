import org.openqa.selenium.*;
import org.testng.*;
import java.util.*;
import org.testng.annotations.*;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
public class PaymentGateway {
	public WebDriver driver;
	JavascriptExecutor js;
	
@BeforeTest
public void start() {	
	System.setProperty("webdriver.chrome.driver", "/home/vela-zstk259/Downloads/chromedriver_linux64/chromedriver");
	driver = new ChromeDriver();
	System.out.println("Launching the browser successfully!");
}

@Test
public void Open() throws InterruptedException {	
	driver.navigate().to("https://demo.guru99.com");
	Thread.sleep(1000);
	driver.manage().window().maximize();
	System.out.println("Given url opened successfully");
}

@Test(priority=1)
public void purchasing() throws InterruptedException{
	driver.findElement(By.linkText("Payment Gateway Project")).click();
	Thread.sleep(6000);
	JavascriptExecutor js = (JavascriptExecutor) driver;
	js.executeScript("window.scrollBy(0,150)", "");  
	Thread.sleep(1000);
	System.out.println("Page scrolled successfully!");
	Select drop=new Select(driver.findElement(By.name("quantity")));
	drop.selectByValue("2");
	System.out.println("Dropdown selected successfully!");
	Thread.sleep(2000);
	driver.findElement(By.cssSelector("input[value=\"Buy Now\"]")).click();
    Thread.sleep(2000);	
    System.out.println("Purchasing window worked perfectly!");
}

@Test(priority=4)
public void detailFill() throws InterruptedException {
	driver.findElement(By.cssSelector("input[name=\"card_nmuber\"]")).sendKeys("1234567890123456"); 
    Thread.sleep(2000);
    System.out.println("Input values are intaked successfully!");
	Select drop2=new Select(driver.findElement(By.name("month")));
	drop2.selectByValue("3");
	Thread.sleep(2000);
	Select drop3=new Select(driver.findElement(By.name("year")));
	drop3.selectByValue("2023");
	Thread.sleep(2000);
	driver.findElement(By.cssSelector("input[name=\"cvv_code\"]")).sendKeys("571"); 
	Thread.sleep(1000);
	JavascriptExecutor js = (JavascriptExecutor) driver;
	js.executeScript("window.scrollBy(0,150)", "");  
	driver.findElement(By.cssSelector("input[name=\"submit\"]")).click();
    Thread.sleep(2000);
    System.out.println("Payment window worked perfectly!");
}

@Test(priority=5)
public void checkCardStatus() throws InterruptedException {
	driver.findElement(By.linkText("Check Credit Card Limit")).click();
	Thread.sleep(1000);
	driver.findElement(By.cssSelector("input[name=\"card_nmuber\"]")).sendKeys("1234567890123456");
	Thread.sleep(1000);
	driver.findElement(By.xpath("//*[@id=\"three\"]/div/form/div/div[6]/input")).click();
	Thread.sleep(1000);
	JavascriptExecutor js = (JavascriptExecutor) driver;
	js.executeScript("window.scrollBy(0,600)", "");  
	Thread.sleep(2000);
	driver.findElement(By.linkText("Cart")).click();
	Thread.sleep(1000); 
	System.out.println("Buyable products add to card & give the balance amount correctly!");
}

@AfterTest
public void close() {	
	driver.close();
	System.out.println("Browser closed successfully!");
}
}

import org.openqa.selenium.*;
import java.util.*;
import org.testng.*;
import org.testng.annotations.*;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;

public class Telegram {
	
	public WebDriver driver; 
	List <WebElement> arrChat;
	List <WebElement> arrMenu;
	
	@BeforeTest
	public void login(){
		System.setProperty("webdriver.gecko.driver","/home/vela-zstk259/Downloads/geckodriver-v0.30.0-linux64/geckodriver");
		driver=new FirefoxDriver();
	}
	 
	@AfterTest
	public void Close() {
		driver.close();
		System.out.println("Browser closed successfully!");
	}
	
	@Test(priority=1) 
	public void Signin() throws InterruptedException{
		driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
		driver.navigate().to("https://web.telegram.org"); 
		driver.manage().window().maximize();
		System.out.println("I am in the correct page!");
		Thread.sleep(10000);
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div/button")).click();
		System.out.println("Sign in page displayed correctly!");
		driver.findElement(By.id("sign-in-phone-code")).sendKeys("Ind");
		Thread.sleep(1000);
		driver.findElement(By.cssSelector("div.MenuItem:nth-child(1)")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("sign-in-phone-number")).sendKeys("9345658589");
		Thread.sleep(1000);
		driver.findElement(By.className("ripple-container")).click();
		WebDriverWait wait=new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("LeftColumn-main")));	
	}
	
	@Test(priority=2)
	public void Chat() throws InterruptedException {   
		System.out.println("Inner page of telegram opened successfully!");
		arrChat=driver.findElements(By.className("Chat"));
		arrChat.get(1).click();
		Thread.sleep(1000);
		driver.findElement(By.id("editable-message-text")).sendKeys("Hai,This message is for automation purpose.");
		Thread.sleep(3000);
		driver.findElement(By.cssSelector(".send")).click();
		System.out.println("Message sent successfully!");
		Thread.sleep(3000);
		driver.navigate().back(); 
		Thread.sleep(1000);
		System.out.println("All navigate buttons worked properly!");
		driver.findElement(By.cssSelector("div.Tab:nth-child(2)")).click();
		Thread.sleep(1000);
		driver.findElement(By.className("ripple-container")).click();
		Thread.sleep(1000);
	}
	
	@Test(priority=3)    
	public void Darkmode() throws InterruptedException {      
		driver.findElement(By.className("Switcher")).click();
		WebElement mode=driver.findElement(By.cssSelector("label[title='Disable night mode']"));
			if (mode.isDisplayed()) {
				System.out.println("Successfully,dark mode is on");
			}
			else {
				System.out.println("Successfully,dark mode is off");
			}
		Thread.sleep(3000);
		driver.findElement(By.className("Switcher")).click();
		mode=driver.findElement(By.cssSelector("label[title='Enable night mode']"));
		if (mode.isDisplayed()) {
			System.out.println("Successfully,dark mode is off");
		}
		else {
			System.out.println("Successfully,dark mode is on");
		}
		Thread.sleep(3000);
	}
	
	@Test(priority=4)
	public void Menu() throws InterruptedException {
		driver.findElement(By.className("icon-saved-messages")).click();
		System.out.println("aved messages shown successfully!");
		Thread.sleep(2000);   
		driver.findElement(By.className("ripple-container")).click();
		System.out.println("Menu bar works successfully!");
		Thread.sleep(1000);  
		driver.findElement(By.className("icon-archive")).click();
		System.out.println("Archive chats shown successfully!");
		Thread.sleep(2000);
		driver.navigate().back();
		Thread.sleep(2000);
	}
	
}

import org.openqa.selenium.By;		
import org.openqa.selenium.WebDriver;		
import org.openqa.selenium.chrome.ChromeDriver;		
import org.openqa.selenium.*;		

public class Radio_Check {				
    public static void main(String[] args) throws InterruptedException {									
    		
    	// declaration and instantiation of objects/variables		
        System.setProperty("webdriver.chrome.driver","/home/vela-zstk259/Downloads/chromedriver_linux64/chromedriver");					
        WebDriver driver = new ChromeDriver();					

        driver.get("http://demo.guru99.com/test/radio.html");	
        Thread.sleep(1000);	
        WebElement radio1 = driver.findElement(By.id("vfb-7-1"));							
        WebElement radio2 = driver.findElement(By.id("vfb-7-2"));							
        		
        //Radio Button1 is selected		
        radio1.click();			
        System.out.println("Radio Button Option 1 Selected");					
        Thread.sleep(1000);	
        
        //Radio Button1 is de-selected and Radio Button2 is selected		
        radio2.click();			
        System.out.println("Radio Button Option 2 Selected");					
        Thread.sleep(1000);	
        
        // Selecting CheckBox		
        WebElement option1 = driver.findElement(By.id("vfb-6-0"));							

        // Click the Check box 		
        option1.click();			
        Thread.sleep(1000);	
        
        // Check whether the Check box is  		
        if (option1.isSelected()) {					
            System.out.println("Checkbox is Toggled On");					

        } else {			
            System.out.println("Checkbox is Toggled Off");					
        }		
        Thread.sleep(1000);		
        		
        //Selecting Checkbox and using isSelected Method		
        driver.get("http://demo.guru99.com/test/facebook.html");					
        WebElement status = driver.findElement(By.id("persist_box"));							
        for (int i=0; i<2; i++) {											
            status.click (); 			
            System.out.println("Facebook Persists Checkbox Status is -  "+status.isSelected());							
        }		
        Thread.sleep(1000);	
		driver.close();		
        		
    }		
}
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.NoAlertPresentException;	
import org.openqa.selenium.Alert;
import java.util.Iterator;		
import java.util.Set;		
import org.openqa.selenium.By;			

public class  FormDelete{
	
	public static void main(String[] args) throws NoAlertPresentException,InterruptedException  {									
		System.setProperty("webdriver.chrome.driver","/home/vela-zstk259/Downloads/chromedriver_linux64/chromedriver");
		WebDriver driver = new ChromeDriver();
        
        // Alert Message handling
                    		
        driver.get("http://demo.guru99.com/test/delete_customer.php");			
                            		
        System.out.println("Browser opens successfully!");	
        driver.findElement(By.name("cusid")).sendKeys("53920");	
        Thread.sleep(2000);
        driver.findElement(By.name("submit")).submit();		
        Thread.sleep(2000);
        		
        // Switching to Alert        
        Alert alert = driver.switchTo().alert();		
        		
        // Capturing alert message.    
        String alertMessage= driver.switchTo().alert().getText();		
        		
        // Displaying alert message		
        System.out.println("alert message is: "+alertMessage);	
        Thread.sleep(2000);
        		
        // Accepting alert		
        alert.accept();	
        System.out.println("Details deleted successfully!");
        Thread.sleep(2000);
        driver.close();
        	
        System.out.println("Browser closed successfully!");	
    }	
}
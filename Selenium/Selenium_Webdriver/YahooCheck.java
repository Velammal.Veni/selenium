import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;  
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
  
public class YahooCheck{  
	private static TimeUnit time;
	public static void main(String[] args) throws InterruptedException  {  
		
// Open browser 
    System.setProperty("webdriver.chrome.driver", "/home/vela-zstk259/Downloads/chromedriver_linux64/chromedriver");
    WebDriver driver=new ChromeDriver();
    driver.manage().window().maximize();
    
// open the Yahoo site
    String baseUrl = "https://login.yahoo.com/account/create";
    driver.navigate().to(baseUrl); 
    
// @Testcase 01
// This is sign up page or not check 
    WebElement element = driver.findElement(By.xpath("//*[@id=\"account-attributes-challenge\"]/h1"));
    String strng = element.getText();
    if(("Sign up").equals(strng)) {
    	System.out.println("This is Sign Up Page! ");
    }
    System.out.println("Test Case 01 is succeed!");
    System.out.println("___________________________________________________________________");
//Webelements declaring
    WebElement name1 = driver.findElement(By.id("usernamereg-firstName"));
	WebElement name2 = driver.findElement(By.id("usernamereg-lastName"));
	WebElement email = driver.findElement(By.id("usernamereg-yid"));
	WebElement button = driver.findElement(By.id("reg-submit-button"));
	WebElement pass = driver.findElement(By.id("usernamereg-password"));
	WebElement pnum = driver.findElement(By.id("usernamereg-phone"));
	WebElement month = driver.findElement(By.id("usernamereg-month"));
	WebElement day = driver.findElement(By.id("usernamereg-day"));
	WebElement year = driver.findElement(By.id("usernamereg-year"));
	
// @ Test case 02
// check by giving valid inputs 
	name1.sendKeys("Velammal");
	name2.sendKeys("Murugan");   
	email.sendKeys("s457687847657");
	pass.sendKeys("'ik3JCu78T5Z+XQ");
	pnum.sendKeys("9345658589");
    month.sendKeys("August");
    day.sendKeys("08");
    year.sendKeys("2003");
    button.submit();
    System.out.println("Test Case 02 is succeed! Accept all valid inputs! ");
    Thread.sleep(2000);
    System.out.println("___________________________________________________________________");
    
// @Test Case 03
// Check by Giving Invalid Input like Special Characters in name field
    driver.navigate().back();
    driver.navigate().refresh(); 
    driver.get(baseUrl); 
    driver.findElement(By.id("usernamereg-firstName")).sendKeys("$$$$$$$$");
    System.out.println("This is First Name=> $$$$$$$$$! ");
	driver.findElement(By.id("usernamereg-lastName")).sendKeys("$$$$$$$$");
	System.out.println("This is Last Name=> $$$$$$$$! ");
	driver.findElement(By.id("usernamereg-yid")).sendKeys("s457687847657");
	driver.findElement(By.id("usernamereg-password")).sendKeys("'ik3JCu78T5Z+XQ");
	driver.findElement(By.id("usernamereg-phone")).sendKeys("9345658589");
	driver.findElement(By.id("usernamereg-month")).sendKeys("August");
    driver.findElement(By.id("usernamereg-day")).sendKeys("08");
    driver.findElement(By.id("usernamereg-year")).sendKeys("2003");
    driver.findElement(By.id("reg-submit-button")).submit();
    System.out.println("Test Case 03 is Failed! Accept the invalid inputs in Name field!");
    Thread.sleep(2000);
    System.out.println("___________________________________________________________________");
    
// @Test Case 04
// Check by Giving Invalid Input like Numbers in name field
    driver.get(baseUrl);
    driver.findElement(By.id("usernamereg-firstName")).sendKeys("4567896789");
    System.out.println("This is First Name => 4567896789! ");
	driver.findElement(By.id("usernamereg-lastName")).sendKeys("4567897");
	System.out.println("This is Last Name => 4567897! ");
	driver.findElement(By.id("usernamereg-yid")).sendKeys("s457687847657");
	driver.findElement(By.id("usernamereg-password")).sendKeys("'ik3JCu78T5Z+XQ");
	driver.findElement(By.id("usernamereg-phone")).sendKeys("9345658589");
	driver.findElement(By.id("usernamereg-month")).sendKeys("August");
    driver.findElement(By.id("usernamereg-day")).sendKeys("08");
    driver.findElement(By.id("usernamereg-year")).sendKeys("2003");
    driver.findElement(By.id("reg-submit-button")).submit();
    System.out.println("Test Case 04 is Failed! Accept the invalid inputs in Name field!");
    Thread.sleep(2000);
    System.out.println("___________________________________________________________________");
    
// @Test Case 05
// Check by Giving Invalid Input like Empty spaces in name field
    driver.get(baseUrl);
    driver.findElement(By.id("usernamereg-firstName")).sendKeys("          ");
	driver.findElement(By.id("usernamereg-lastName")).sendKeys("           ");
	driver.findElement(By.id("usernamereg-yid")).sendKeys("s457687847657");
	driver.findElement(By.id("usernamereg-password")).sendKeys("'ik3JCu78T5Z+XQ");
	driver.findElement(By.id("usernamereg-phone")).sendKeys("9345658589");
	driver.findElement(By.id("usernamereg-month")).sendKeys("August");
    driver.findElement(By.id("usernamereg-day")).sendKeys("08");
    driver.findElement(By.id("usernamereg-year")).sendKeys("2003");
    driver.findElement(By.id("reg-submit-button")).submit();
    System.out.println("Test Case 05 is succeed! not Accept the Empty space in Name field!");
    Thread.sleep(2000);
    System.out.println("___________________________________________________________________");
    
// @Test Case 06
// Check by Giving Invalid Input like lengthy name in name field
    driver.get(baseUrl);
    driver.findElement(By.id("usernamereg-firstName")).sendKeys("asdfghjkfgtyuojytsgyhibdgtyisasv");
	driver.findElement(By.id("usernamereg-lastName")).sendKeys("fdghrtyidhdhfgggdgdhehueljdnvchcdfg");
	driver.findElement(By.id("usernamereg-yid")).sendKeys("s457687847657");
	driver.findElement(By.id("usernamereg-password")).sendKeys("'ik3JCu78T5Z+XQ");
	driver.findElement(By.id("usernamereg-phone")).sendKeys("9345658589");
	driver.findElement(By.id("usernamereg-month")).sendKeys("August");
    driver.findElement(By.id("usernamereg-day")).sendKeys("08");
    driver.findElement(By.id("usernamereg-year")).sendKeys("2003");
    driver.findElement(By.id("reg-submit-button")).submit();
    System.out.println("Test Case 06 is succeed! not Accept the  inputs in Name field!");
    Thread.sleep(2000);
    System.out.println("___________________________________________________________________");
    
// @Test Case 07
// Check by Giving Invalid Input like short name in name field
    driver.get(baseUrl);
    driver.findElement(By.id("usernamereg-firstName")).sendKeys("asd");
	driver.findElement(By.id("usernamereg-lastName")).sendKeys("fdg");
	driver.findElement(By.id("usernamereg-yid")).sendKeys("s457687847657");
	driver.findElement(By.id("usernamereg-password")).sendKeys("'ik3JCu78T5Z+XQ");
	driver.findElement(By.id("usernamereg-phone")).sendKeys("9345658589");
	driver.findElement(By.id("usernamereg-month")).sendKeys("August");
    driver.findElement(By.id("usernamereg-day")).sendKeys("08");
    driver.findElement(By.id("usernamereg-year")).sendKeys("2003");
    driver.findElement(By.id("reg-submit-button")).submit();
    System.out.println("Test Case 07 is failed! Accept the invalid inputs in Name field!");
    Thread.sleep(2000);
    System.out.println("___________________________________________________________________");
    
// @Test Case 08
// Invalid email  (numbers giving)
    driver.get(baseUrl);
    driver.findElement(By.id("usernamereg-firstName")).sendKeys("asdsdfgh");
	driver.findElement(By.id("usernamereg-lastName")).sendKeys("fdgcvbn");
	driver.findElement(By.id("usernamereg-yid")).sendKeys("457687847657");
	System.out.println("This is invalid mailid=> 457687847657! ");
	driver.findElement(By.id("usernamereg-password")).sendKeys("'ik3JCu78T5Z+XQ");
	driver.findElement(By.id("usernamereg-phone")).sendKeys("9345658589");
	driver.findElement(By.id("usernamereg-month")).sendKeys("August");
    driver.findElement(By.id("usernamereg-day")).sendKeys("08");
    driver.findElement(By.id("usernamereg-year")).sendKeys("2003");
    driver.findElement(By.id("reg-submit-button")).submit();
    System.out.println("Test Case 08 is Succeed! not Accept the invalid inputs in email!");
    Thread.sleep(2000);
    System.out.println("___________________________________________________________________");
    
// @Test Case 09
//Invalid email  (numbers empty space)
    driver.get(baseUrl);
    driver.findElement(By.id("usernamereg-firstName")).sendKeys("asdsdfgh");
	driver.findElement(By.id("usernamereg-lastName")).sendKeys("fdgcvbn");
	driver.findElement(By.id("usernamereg-yid")).sendKeys("   ");
	System.out.println("This is invalid mailid=> empty space! ");
	driver.findElement(By.id("usernamereg-password")).sendKeys("'ik3JCu78T5Z+XQ");
	driver.findElement(By.id("usernamereg-phone")).sendKeys("9345658589");
	driver.findElement(By.id("usernamereg-month")).sendKeys("August");
    driver.findElement(By.id("usernamereg-day")).sendKeys("08");
    driver.findElement(By.id("usernamereg-year")).sendKeys("2003");
    driver.findElement(By.id("reg-submit-button")).submit();
    System.out.println("Test Case 09 is Passed! not Accept the invalid inputs in email!");
    Thread.sleep(2000);
    System.out.println("___________________________________________________________________"); 
	
// @Test Case 10
// Invalid password check
    driver.get(baseUrl);
    driver.findElement(By.id("usernamereg-firstName")).sendKeys("asdsdfgh");
	driver.findElement(By.id("usernamereg-lastName")).sendKeys("fdgcvbn");
	driver.findElement(By.id("usernamereg-yid")).sendKeys("sdfghj34567890");
	driver.findElement(By.id("usernamereg-password")).sendKeys("'3457823456747");
	System.out.println("This is invalid password contains only numbers! ");
	driver.findElement(By.id("usernamereg-phone")).sendKeys("9345658589");
	driver.findElement(By.id("usernamereg-month")).sendKeys("August");
    driver.findElement(By.id("usernamereg-day")).sendKeys("08");
    driver.findElement(By.id("usernamereg-year")).sendKeys("2003");
    driver.findElement(By.id("reg-submit-button")).submit();
    System.out.println("Test Case 10 is Failed! Accept only numeric values!");
    Thread.sleep(2000);
    System.out.println("___________________________________________________________________");
    
// @Test Case 11
// Invalid Password check 
    driver.get(baseUrl);
    driver.findElement(By.id("usernamereg-firstName")).sendKeys("asdsdfgh");
	driver.findElement(By.id("usernamereg-lastName")).sendKeys("fdgcvbn");
	driver.findElement(By.id("usernamereg-yid")).sendKeys("sdfghj34567890");
	driver.findElement(By.id("usernamereg-password")).sendKeys("'$$$$$$345676##$");
	System.out.println("This is invalid password! ");
	driver.findElement(By.id("usernamereg-phone")).sendKeys("9345658589");
	driver.findElement(By.id("usernamereg-month")).sendKeys("August");
    driver.findElement(By.id("usernamereg-day")).sendKeys("08");
    driver.findElement(By.id("usernamereg-year")).sendKeys("2003");
    driver.findElement(By.id("reg-submit-button")).submit();
    System.out.println("Test Case 11 is Failed! Accept only number & special characters input!");
    Thread.sleep(2000);
    System.out.println("________________________________________________________________");
 
// @Test Case 12
// Invalid Password check 
    driver.get(baseUrl);
    driver.findElement(By.id("usernamereg-firstName")).sendKeys("asdsdfgh");
	driver.findElement(By.id("usernamereg-lastName")).sendKeys("fdgcvbn");
	driver.findElement(By.id("usernamereg-yid")).sendKeys("sdfghj34567890");
	driver.findElement(By.id("usernamereg-password")).sendKeys("        ");
	System.out.println("This is invalid password! ");
	driver.findElement(By.id("usernamereg-phone")).sendKeys("9345658589");
	driver.findElement(By.id("usernamereg-month")).sendKeys("August");
    driver.findElement(By.id("usernamereg-day")).sendKeys("08");
    driver.findElement(By.id("usernamereg-year")).sendKeys("2003");
    driver.findElement(By.id("reg-submit-button")).submit();
    System.out.println("Test Case 12 is Passed! not Accept the empty space!");
    Thread.sleep(2000);
    System.out.println("___________________________________________________________________");
    
// @Test Case 13
// Invalid Phone number check  
    driver.get(baseUrl);
    driver.findElement(By.id("usernamereg-firstName")).sendKeys("asdsdfgh");
	driver.findElement(By.id("usernamereg-lastName")).sendKeys("fdgcvbn");
	driver.findElement(By.id("usernamereg-yid")).sendKeys("sdfghj34567890");
	driver.findElement(By.id("usernamereg-password")).sendKeys("234567899098");
	driver.findElement(By.id("usernamereg-phone")).sendKeys("9946756567707");
	System.out.println("This is invalid mobile long number! ");
	driver.findElement(By.id("usernamereg-month")).sendKeys("August");
    driver.findElement(By.id("usernamereg-day")).sendKeys("03");
    driver.findElement(By.id("usernamereg-year")).sendKeys("2003");
    driver.findElement(By.id("reg-submit-button")).submit();
    System.out.println("Test Case 13 is Passed! not Accept inavlid number!");
    Thread.sleep(2000);
    System.out.println("___________________________________________________________________");
    
// @Test Case 14
// Invalid Phone number check 
    driver.get(baseUrl);
    driver.findElement(By.id("usernamereg-firstName")).sendKeys("asdsdfgh");
	driver.findElement(By.id("usernamereg-lastName")).sendKeys("fdgcvbn");
	driver.findElement(By.id("usernamereg-yid")).sendKeys("sdfghj34567890");
	driver.findElement(By.id("usernamereg-password")).sendKeys("234567899098");
	driver.findElement(By.id("usernamereg-phone")).sendKeys("994675678");
	System.out.println("This is invalid mobile short numbers! ");
	driver.findElement(By.id("usernamereg-month")).sendKeys("August");
    driver.findElement(By.id("usernamereg-day")).sendKeys("03");
    driver.findElement(By.id("usernamereg-year")).sendKeys("2003");
    driver.findElement(By.id("reg-submit-button")).submit();
    System.out.println("Test Case 14 is Passed! not Accept short numbers!");
    Thread.sleep(2000);
    System.out.println("___________________________________________________________________");
    
//@Test Case 15
// Invalid Birth Date check 
    driver.get(baseUrl);
    driver.findElement(By.id("usernamereg-firstName")).sendKeys("asdsdfgh");
	driver.findElement(By.id("usernamereg-lastName")).sendKeys("fdgcvbn");
	driver.findElement(By.id("usernamereg-yid")).sendKeys("sdfghj34567890");
	driver.findElement(By.id("usernamereg-password")).sendKeys("234567899098");
	driver.findElement(By.id("usernamereg-phone")).sendKeys("9345658589");
	driver.findElement(By.id("usernamereg-month")).sendKeys("February");
    driver.findElement(By.id("usernamereg-day")).sendKeys("29");
    driver.findElement(By.id("usernamereg-year")).sendKeys("2003");
    System.out.println("This is invalid Birth Date! ");
    driver.findElement(By.id("reg-submit-button")).submit();
    System.out.println("Test Case 15 is Passed! not Accept invalid date!");
    Thread.sleep(2000);
    System.out.println("___________________________________________________________________");
    
// @Test Case 16
// Invalid Gender check 
    driver.get(baseUrl);
    driver.findElement(By.id("usernamereg-firstName")).sendKeys("asdsdfgh");
	driver.findElement(By.id("usernamereg-lastName")).sendKeys("fdgcvbn");
	driver.findElement(By.id("usernamereg-yid")).sendKeys("sdfghj34567890");
	driver.findElement(By.id("usernamereg-password")).sendKeys("234567899098");
	driver.findElement(By.id("usernamereg-phone")).sendKeys("9345658589");
	driver.findElement(By.id("usernamereg-month")).sendKeys("February");
    driver.findElement(By.id("usernamereg-day")).sendKeys("28");
    driver.findElement(By.id("usernamereg-year")).sendKeys("2003");
    driver.findElement(By.id("usernamereg-freeformGender")).sendKeys("asdfghjk");
    System.out.println("This is invalid Gender! ");
    driver.findElement(By.id("reg-submit-button")).submit();
    System.out.println("Test Case 16 is Failed!  Accept the all keywords!");
    Thread.sleep(2000);
    System.out.println("___________________________________________________________________");
    
// Verify title
    if(driver.getTitle().contentEquals("Yahoo")) {
    	System.out.println("This site is Yahoo");
    } 
    else {
        System.out.println("This site is not Yahoo");
    }   
	Thread.sleep(2000);
	driver.close();
	}
}


